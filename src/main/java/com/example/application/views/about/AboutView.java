package com.example.application.views.about;

import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.PageTitle;
import com.example.application.views.main.MainView;
import com.vaadin.flow.component.dependency.CssImport;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

@Route(value = "about", layout = MainView.class)
@PageTitle("About")
@CssImport("./views/about/about-view.css")
public class AboutView extends Div {

    public AboutView() {
        addClassName("about-view");
        add(new Text("Contact Form"));

        TextField labelField = new TextField();
        labelField.setLabel("Name");

        TextField valueField = new TextField();
        valueField.setValue("Value");

        TextField labelField1 = new TextField();
        labelField1.setLabel("E-mail");

        TextField labelField3 = new TextField();
        labelField3.setLabel("Message");

        add(labelField, labelField1, labelField3, labelField3);

        Button b = new Button("Submit");
        b.addClickListener(clickEvent -> {
            Notification.show("success");

            URL url = null;
            try {
                url = new URL("https://jn4xwas9ck.execute-api.us-east-2.amazonaws.com/default/emailTest");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            try {
                HttpURLConnection con = (HttpURLConnection)url.openConnection();
                con.setRequestMethod("POST");
                con.setRequestProperty("Content-Type", "application/json");
                con.setRequestProperty("Accept", "application/json");
                con.setDoOutput(true);

                String jsonInputString = "{\"senderName\": \"" + labelField.getValue() + "\", \"senderEmail\": \"" + labelField1.getValue() + "\", \"message\": \"" + labelField3.getValue() + "\"}";

                try(OutputStream os = con.getOutputStream()) {
                    byte[] input = jsonInputString.getBytes("utf-8");
                    os.write(input, 0, input.length);
                }

                try(BufferedReader br = new BufferedReader(
                        new InputStreamReader(con.getInputStream(), "utf-8"))) {
                    StringBuilder response = new StringBuilder();
                    String responseLine = null;
                    while ((responseLine = br.readLine()) != null) {
                        response.append(responseLine.trim());
                    }
                    System.out.println(response.toString());

                }
                Notification.show("success");

            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        add(b);
    }
}
